package org.cesi.sexeshop.dao;

import org.cesi.sexeshop.entity.Account;

public interface AccountDAO {
	public Account findAccount(String userName );
}
