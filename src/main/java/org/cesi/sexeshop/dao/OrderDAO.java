package org.cesi.sexeshop.dao;


import java.util.List;

import org.cesi.sexeshop.model.CartInfo;
import org.cesi.sexeshop.model.OrderDetailInfo;
import org.cesi.sexeshop.model.OrderInfo;
import org.cesi.sexeshop.model.PaginationResult;

public interface OrderDAO {
	public void saveOrder(CartInfo cartInfo);

	public PaginationResult<OrderInfo> listOrderInfo(int page, int maxResult, int maxNavigationPage);

	public OrderInfo getOrderInfo(String orderId);

	public List<OrderDetailInfo> listOrderDetailInfos(String orderId);
}
