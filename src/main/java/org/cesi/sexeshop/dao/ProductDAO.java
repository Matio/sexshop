package org.cesi.sexeshop.dao;

import org.cesi.sexeshop.entity.Product;
import org.cesi.sexeshop.model.PaginationResult;
import org.cesi.sexeshop.model.ProductInfo;

public interface ProductDAO {
	public Product findProduct(String code);

	public ProductInfo findProductInfo(String code);

	public PaginationResult<ProductInfo> queryProducts(int page, int maxResult, int maxNavigationPage);

	public PaginationResult<ProductInfo> queryProducts(int page, int maxResult, int maxNavigationPage, String likeName);

	public void save(ProductInfo productInfo);
}
