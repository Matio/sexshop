<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>

<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="security"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	<a class="navbar-brand" href="${pageContext.request.contextPath}/">Paradis Shop</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#navbarText" aria-controls="navbarText"
		aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarText">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item"><a class="nav-link" href="${pageContext.request.contextPath}/productList">Catalogue</a></li>
			<li class="nav-item"><a class="nav-link" href="${pageContext.request.contextPath}/shoppingCart">Mon panier</a></li>
			<li  class="nav-item">
				<security:authorize access="hasAnyRole('ROLE_MANAGER','ROLE_EMPLOYEE')">
					<a class="nav-link" href="${pageContext.request.contextPath}/orderList"> Liste de commande</a>
   				</security:authorize>
 			</li>
 			<li  class="nav-item">
 				<security:authorize access="hasRole('ROLE_MANAGER')">
					<a class="nav-link" href="${pageContext.request.contextPath}/product">Créer Produit </a>
   				</security:authorize>
 			</li>
 			 <c:if test="${pageContext.request.userPrincipal.name != null}">
	 			<li class="nav-item">
	 				<a class="nav-link" href="${pageContext.request.contextPath}/accountInfo">Bonjour ${pageContext.request.userPrincipal.name} </a>
	 			</li>
	 			<li class="nav-item">
	 				<a class="nav-link" href="${pageContext.request.contextPath}/logout">Déconnexion</a>
	 			</li>
 			</c:if>
 			<c:if test="${pageContext.request.userPrincipal.name == null}">
	 			<li class="nav-item">
	 				<a class="nav-link" href="${pageContext.request.contextPath}/login">Connexion</a>
	 			</li>
 			</c:if>
		</ul>
	</div>
</nav>