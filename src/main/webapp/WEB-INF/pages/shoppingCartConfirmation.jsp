<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<title>Paradis Shop - Confirmation</title>

<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}\asset\css\style.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}\asset\bootstrap\css\bootstrap.min.css">

</head>
<body>

	<jsp:include page="_menu.jsp" />

	<fmt:setLocale value="en_FR" scope="session" />

	<div class="customer-info-container">
		<h3>Information:</h3>
		<div>
			<p>Nom : ${myCart.customerInfo.name}</p>
			<p>Email : ${myCart.customerInfo.email}</p>
			<p>Telephone : ${myCart.customerInfo.phone}</p>
			<p>Addresse : ${myCart.customerInfo.address}</p>
		</div>
		<h3>Achat:</h3>
		<div>
			<p>Quantit�: ${myCart.quantityTotal}</p>
			<p>
				Total : <span class="total"> <fmt:formatNumber
						value="${myCart.amountTotal}" type="currency" />
				</span>
			</p>
		</div>
	</div>

	<form method="POST"
		action="${pageContext.request.contextPath}/shoppingCartConfirmation">

		<a class="navi-item"
			href="${pageContext.request.contextPath}/shoppingCart">Editer
			Panier</a> <a class="navi-item"
			href="${pageContext.request.contextPath}/shoppingCartCustomer">Editer
			info</a> <input type="submit" value="Confirmer" class="button-send-sc" />
	</form>

	<div class="container-product">

		<c:forEach items="${myCart.cartLines}" var="cartLineInfo">
			<div class="card products" style="width: 18rem;">
				<img class="card-img-top"
					src="${pageContext.request.contextPath}/productImage?code=${cartLineInfo.productInfo.code}" />
				<div class="card-body">
					<h5 class="card-title">${cartLineInfo.productInfo.name}</h5>
					<p>
						Code: ${cartLineInfo.productInfo.code} <input type="hidden"
							name="code" value="${cartLineInfo.productInfo.code}" />
					</p>
					<p>
						<fmt:formatNumber value="${cartLineInfo.productInfo.price}"
							type="currency" />
					</p>
					<p>Quantit�: ${cartLineInfo.quantity}</p>
					<p>
						Total: <span class="subtotal"> <fmt:formatNumber
								value="${cartLineInfo.amount}" type="currency" />
						</span>
					</p>
				</div>
			</div>
		</c:forEach>

	</div>

</body>
</html>