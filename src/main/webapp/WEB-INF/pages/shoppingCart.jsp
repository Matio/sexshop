<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<title>Paradis Shop - Panier</title>

<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}\asset\css\style.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}\asset\bootstrap\css\bootstrap.min.css">

</head>
<body>

	<jsp:include page="_menu.jsp" />

	<fmt:setLocale value="en_FR" scope="session" />


	<c:if test="${empty cartForm or empty cartForm.cartLines}">
		<h2>Il n'y a pas de produits dans votre panier</h2>
		<a href="${pageContext.request.contextPath}/productList">Catalogue</a>
	</c:if>

	<c:if test="${not empty cartForm and not empty cartForm.cartLines   }">
		<form:form method="POST" modelAttribute="cartForm"
			action="${pageContext.request.contextPath}/shoppingCart">

			<div class="product-preview-container">
				<c:forEach items="${cartForm.cartLines}" var="cartLineInfo"
					varStatus="varStatus">

					<div class="card products" style="width: 18rem;">
						<img class="card-img-top"
							src="${pageContext.request.contextPath}/productImage?code=${cartLineInfo.productInfo.code}" />
						<div class="card-body">
							<h5 class="card-title">${cartLineInfo.productInfo.name}</h5>
							<p class="card-text">
								Code: ${cartLineInfo.productInfo.code}
								<form:hidden
									path="cartLines[${varStatus.index}].productInfo.code" />
							</p>

							<p class="card-text">
								Prix: <span class="price"> <fmt:formatNumber
										value="${cartLineInfo.productInfo.price}" type="currency" />
							</p>

							<p class="card-text">
								Sous total: <span class="subtotal"> <fmt:formatNumber
										value="${cartLineInfo.amount}" type="currency" />
							</p>

							<p class="card-text">
								Quantit�:
								<form:input path="cartLines[${varStatus.index}].quantity" />
							</p>

							<a class="btn btn-danger"
								href="${pageContext.request.contextPath}/shoppingCartRemoveProduct?code=${cartLineInfo.productInfo.code}">
								Supprimer </a>

						</div>
					</div>
				</c:forEach>
			</div>

			<div style="clear: both"></div>

			<div style="margin:auto; width:26%;">
				<input class="btn btn-primary" type="submit"
					value="Modifier Quantit�" /> <a class="btn btn-info"
					href="${pageContext.request.contextPath}/shoppingCartCustomer">
					Acheter</a> <a class="btn btn-success"
					href="${pageContext.request.contextPath}/productList">Continuer
					Achat</a>
			</div>
		</form:form>
	</c:if>
</body>
</html>