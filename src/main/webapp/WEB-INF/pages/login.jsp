<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<title>Paradis Shop - Connexion</title>

<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}\asset\css\style.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}\asset\bootstrap\css\bootstrap.min.css">

</head>
<body>

	<jsp:include page="_menu.jsp" />
	<div class="login-container">

		<h3>Connexion Employe et manager</h3>
		<br>

		<form method="POST"
			action="${pageContext.request.contextPath}/j_spring_security_check">
			<table>
				<tr>
					<td>Nom d'utilisateur *</td>
					<td><input name="userName" /></td>
				</tr>

				<tr>
					<td>Mot de passe *</td>
					<td><input type="password" name="password" /></td>
				</tr>

				<tr>
					<td>&nbsp;</td>
					<td><input type="submit" value="Login" /> <input type="reset"
						value="Reset" /></td>
				</tr>
			</table>
		</form>

		<c:if test="${param.error == 'true'}">
			<div style="color: red; margin: 10px 0px;">

				Connexion �chou�e<br />
				${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}

			</div>
		</c:if>

		<span class="error-message">${error }</span>

	</div>

</body>
</html>