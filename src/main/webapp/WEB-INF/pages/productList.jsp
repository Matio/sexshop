<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="security"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Paradis Shop -Catalogue</title>

<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}\asset\css\style.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}\asset\bootstrap\css\bootstrap.min.css">

</head>
<body>
	<jsp:include page="_menu.jsp" />

	<fmt:setLocale value="en_FR" scope="session" />
	<div class="product-container">
		<c:forEach items="${paginationProducts.list}" var="prodInfo">
				<div class="card products" style="width: 18rem;">
					<img class="card-img-top" src="${pageContext.request.contextPath}/productImage?code=${prodInfo.code}" />
					<div class="card-body">
						<h5 class="card-title">${prodInfo.name}</h5>
						<p><fmt:formatNumber value="${prodInfo.price}" type="currency" /></p>
						<a href="${pageContext.request.contextPath}/buyProduct?code=${prodInfo.code}" class="btn btn-primary">Acheter</a>
						<security:authorize access="hasRole('ROLE_MANAGER')">
							<p><a style="color: red;" href="${pageContext.request.contextPath}/product?code=${prodInfo.code}">Editer produit</a></p>
						</security:authorize>
					</div>
				</div>
		</c:forEach>
	</div>
	<br />


	<c:if test="${paginationProducts.totalPages > 1}">
		<div class="page-navigator">
			<c:forEach items="${paginationProducts.navigationPages}" var="page">
				<c:if test="${page != -1 }">
					<a href="productList?page=${page}" class="nav-item">${page}</a>
				</c:if>
				<c:if test="${page == -1 }">
					<span class="nav-item"> ... </span>
				</c:if>
			</c:forEach>

		</div>
	</c:if>
</body>
</html>