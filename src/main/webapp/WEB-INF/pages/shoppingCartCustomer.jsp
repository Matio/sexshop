<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<title>Paradis Shop - Info panier</title>

<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}\asset\css\style.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}\asset\bootstrap\css\bootstrap.min.css">

</head>
<body>

	<jsp:include page="_menu.jsp" />

	<form:form method="POST" modelAttribute="customerForm"
		action="${pageContext.request.contextPath}/shoppingCartCustomer">

		<table
			style="text-align: left; width: 35%; height: 100%; margin: 5%;">
			<tr>
				<td><h2>Rentrez vos infos :</h2></td>
			</tr>
			<tr>
				<td>Name *</td>
				<td><form:input path="name" /></td>
				<td><form:errors path="name" class="error-message" /></td>
			</tr>

			<tr>
				<td>Email *</td>
				<td><form:input path="email" /></td>
				<td><form:errors path="email" class="error-message" /></td>
			</tr>

			<tr>
				<td>Phone *</td>
				<td><form:input path="phone" /></td>
				<td><form:errors path="phone" class="error-message" /></td>
			</tr>

			<tr>
				<td>Address *</td>
				<td><form:input path="address" /></td>
				<td><form:errors path="address" class="error-message" /></td>
			</tr>

			<tr>
				<td>&nbsp;</td>
				<td><input type="submit" value="Envoyer" /> <input
					type="reset" value="Reset" /></td>
			</tr>
		</table>

	</form:form>
</body>
</html>