<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
	<head>
	<meta charset="UTF-8">
	
	<title>Paradis Shop</title>
	
	<style>
            #container {
                border: 1px #999999 solid;
                padding: 10px;
            }
            p.client {
                border-bottom: 1px aquamarine solid;
            }
            p.server {
                border-bottom: 1px crimson solid;
            }
            input {
                padding: 5px;
                width: 250px;
            }
            button {
                padding: 5px;
            }
        </style>
        <script>
            var chatClient = new WebSocket("ws://localhost:8080/Sexeshop/websocketendpoint");
            
            chatClient.onmessage = function(evt) {
                var p = document.createElement("p");
                p.setAttribute("class", "server");
                p.innerHTML = "Server: " + evt.data;
                var container = document.getElementById("container");
                container.appendChild(p);
            }
            function send() {
                var input = document.getElementById("message");
                var p = document.createElement("p");
                p.setAttribute("class", "client");
                p.innerHTML = "Me: " + input.value;
                var container = document.getElementById("container");
                container.appendChild(p);
                chatClient.send(input.value);
                input.value = "";
            }
        </script>
	
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}\asset\css\style.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}\asset\bootstrap\css\bootstrap.min.css">
	</head>
	<body>
		<jsp:include page="_menu.jsp" />
	
		<div class="container">
			<a href="${pageContext.request.contextPath}/productList"><img id="galerie" src="${pageContext.request.contextPath}\asset\images\sexeshop.PNG" /></a>
		</div>
		
		<div id="container">
            
        </div>
        <input type="text" id="message" name="message" />
        <button type="button" id="send" onclick="send()">Send</button>
	
	</body>
</html>