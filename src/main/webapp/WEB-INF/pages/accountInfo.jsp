<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="security"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<title>Paradis shop - Profils</title>

<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}\asset\css\style.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}\asset\bootstrap\css\bootstrap.min.css">

</head>
<body>
	<jsp:include page="_menu.jsp" />

	<div class="account-container">

		<h2>Information de compte</h2>
		<div>
			<p>Nom d'utilisateur: ${pageContext.request.userPrincipal.name}</p>
			<div>Role:
				<div>
					<c:forEach items="${userDetails.authorities}" var="auth">
						<p>${auth.authority }</p>
					</c:forEach>
				</div>
			</div>
		</div>
	</div>
</body>
</html>