<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
 
<title>Finalisation achat</title>
 
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}\asset\css\style.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}\asset\bootstrap\css\bootstrap.min.css">
 
</head>
<body>
 
   <jsp:include page="_menu.jsp" />
  
   <div class="container">
       <h3>Merci pour votre achat</h3>
      Num�ro de commande: ${lastOrderedCart.orderNum}
   </div>
 
</body>
</html>